%% UNIFIEO - CCONA7 - Paradigmas de Linguagem de Programação
%% Projeto Chat Erlang:
%% Alunos:
	%% Rodrigo Rodrigues Guilherme
	%% Guilherme Augusto Malaquias

%%% Módulo SERVIDOR

-module(sservidor).
-compile(export_all).
-include_lib("stdlib/include/qlc.hrl").

-record(registro, {nome,socket}).
-record(mensagem, {remetente, destinatario, msg}).
-record(lista, {usuarios}).

iniciadatabase()->
	cria_database(),
	inicia_database(),
	inicia().



inicia()->
	{ok, Listen} = gen_tcp:listen(2345, [binary, {packet,0}, {active, true}, {reuseaddr, true}]),
	inicia2(Listen).

inicia2(Listen) ->
	{ok, Socket} = gen_tcp:accept(Listen),	
	spawn (sservidor, inicia2, [Listen]),
	loop(Socket).


loop(Socket)->
		receive 
		{tcp, Socket, Dado} ->
			io:format("~w~n",[Dado]),
			D = binary_to_term(Dado),
			io:format("~w~n",[D]),
			case binary_to_term(Dado) of
				{registro,Nome} ->
					insert_chat(Nome, Socket),
					% gravar Nome,Socket na base
					io:format("Usuario ~s gravado com sucesso.~n", [Nome]),
					Conectados = select_all(),
					DadoEnvio = #lista{usuarios = Conectados},
					gen_tcp:send(Socket, term_to_binary(DadoEnvio)),
					ok;		

					
				{mensagem, Remetente, Destinatario, Mensagem} ->
					Dest = verifica_destinatario(Destinatario),
					case Dest of
						[] ->
							%nao achou nada na base
							erro;
						_ ->
							[ConectadosAgora] = Dest,
							[Rem]=select(Remetente),
							io:fwrite("~w,~w" ,[Rem , ConectadosAgora]),
								if 						
									ConectadosAgora == Destinatario ->
										[Sck]=select(Destinatario),
										io:format("Socket ~w do destinatario ~s~n", [Sck, Destinatario]),
										M = #mensagem{msg = Mensagem, remetente = Remetente, destinatario = Destinatario},
										gen_tcp:send(Sck, term_to_binary(M));
							
									ConectadosAgora /= Destinatario ->
										Ok = {ok,"Destinatario nao esta conectado"},	
										gen_tcp:send(Rem, Ok);
							
									true ->
										0
					
									end
								end
					
						end,

			loop(Socket)	
			
		end.

chat(Socket)->%% copiar o trecho do codigo para chamar dentro do connect
	{ok, [M]}=io:fread("Digite a mensagem: ","~s"),
	gen_tcp:send(Socket,M).%% passando a mensagem para o servidor
	
chatreceive(Socket)->
		receive
		{tcp, Socket, Data} -> %% mensagem chega na variavel Data

			D = binary_to_list(Data), %% declarando uma variavel para receber a confirmação do servidor e mostrar a mensagem

			io:format("Mensagem Recebida:  ~s~n", [D]), %% mostrar mensagem

	
	
	chat(Socket)
 end.

cria_database()->
	mnesia:create_schema([node()]),
	mnesia:start(),
	mnesia:create_table(registro,[{attributes, record_info(fields, registro)}]),
	mnesia:create_table(mensagem,[{attributes, record_info(fields, mensagem)}]),
	mnesia:change_table_copy_type(cregistro, node(), disc_copies),
	mnesia:change_table_copy_type(mensagem, node(), disc_copies),
	mnesia:stop().

inicia_database()->
	mnesia:start(),
	mnesia:wait_for_tables([registro,mensagem], 200).

fecha_database()->
	mnesia:stop().

insert_chat(Nome, Socket)->
	Linha = novo_chat(Nome, Socket),	
	F = fun()-> mnesia:write(Linha) end,
	mnesia:transaction(F).

novo_chat(Nome, Socket)->
	Chat=#registro{nome=Nome, socket=Socket},
	Chat.

do_query(Query)->
	F=fun()-> qlc:e(Query) end,
	{atomic, Val} = mnesia:transaction(F),
	Val.


select(Nome)->
	do_query(qlc:q([X#registro.socket || X <-mnesia:table(registro), X#registro.nome =:= Nome])).

select_all()->
	do_query(qlc:q([X#registro.nome || X<- mnesia:table(registro)])).


verifica_destinatario(Nome)->
	do_query(qlc:q([X#registro.nome || X <-mnesia:table(registro), X#registro.nome =:= Nome])).		



