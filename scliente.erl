%% UNIFIEO - CCONA7 - Paradigmas de Linguagem de Programação
%% Projeto Chat Erlang:
%% Alunos:
	%% Rodrigo Rodrigues Guilherme
	%% Guilherme Augusto Malaquias

%%% Módulo Cliente

-module(scliente).
-compile(export_all).

-record(registro,{nome}).
-record(mensagem,{nome, destinatario, mens}).
-record(lista, {usuarios}).

%Modulo de chamada
connect()->

	{ok, Socket} = gen_tcp:connect('localhost', 2345,[binary,{active, true}, {packet, 0}]),
	{ok, [Nome]}=io:fread("Digite seu nome: ","~s"),
	D=#registro{nome = Nome},	
	gen_tcp:send(Socket, term_to_binary(D)),
	spawn(scliente, chatreceive, [Socket, Nome]),
	io:fwrite("Conexao Estabelecida~n~n"),
	recebelista(Socket),
	chat(Socket, Nome).	
	
	

chat(Socket, Nome)->         
	{ok, [Dest]}=io:fread("Digite o destinatario: ","~s"),
	M = io:get_line("Digite a mensagem: "),
	D = #mensagem{nome = Nome, destinatario = Dest, mens = M},
	io:format("[~s]~n", [M]),
	gen_tcp:send(Socket, term_to_binary(D)),        %% passando a mensagem para o servidor
	chatreceive(Socket, Nome),
	chat(Socket, Nome).
	
chatreceive(Socket, Nome)->
	receive
		{tcp, Socket, Data} -> %% mensagem chega na variavel Data
			D = binary_to_term(Data), %% declarando uma variavel para receber a confirmação do servidor e mostrar a mensagem
			io:format("~p~n" , [D]),
			#mensagem{mens = Msg, nome = Nome2} = D,
			{{_,_,_},{H,MM,S}} = calendar:local_time(),
			io:format("Mensagem Recebida de: [~s] as ~p:~p:~p ~n", [Nome2,H,MM,S]),
			io:format("Mensagem: ~s~n", [Msg]), %% mostrar mensagem
			%chatreceive(Socket, Nome),
			chat(Socket,Nome)
	end.
	
recebelista(Socket)->
		receive
			{tcp, Socket, Data} -> %% mensagem chega na variavel Data
				N = binary_to_term(Data), %% declarando uma variavel para receber a confirmação do servidor e mostrar a mensagem
				#lista{usuarios = Usuarios} = N,
				io:format("Clientes Conectados:  ~s~n", [Usuarios]) %% mostrar mensagem
		end.	

	

	